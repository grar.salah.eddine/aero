import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import {  FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { getDeepFromObject, NbAuthService, NbAuthSimpleToken, NbAuthStrategyOptions, NbPasswordStrategyMessage, NbPasswordStrategyModule, NbPasswordStrategyReset, NbPasswordStrategyToken } from '@nebular/auth';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class NgxLoginComponent implements OnInit {
  identifier = new FormControl();
  password = new FormControl();
  loading = false;
  showMessages = { error: false, success: false };
  submitted;
  messages = [];
  errors = [];
  constructor(
    // public dialog: MatDialog, private autho: AuthenticationService,
    // private communicationMain: CommunicationGestionCourrielServiceService,
     private router: Router,
     private NbAuthService : NbAuthService
  ) { }

  ngOnInit() {
  }

  Registre() {
   
    // const dialogRef = this.dialog.open(RegistreComponent, {
    //   width: '800px',
    //   maxHeight: '900px',
    // });
    // dialogRef.afterClosed().toPromise().then(resultPopup => {
    // });
  }

  async authenticate() {
 
    this.NbAuthService.authenticate('email',{identifier : this.identifier.value , password : this.password.value}).toPromise().then(result =>{
      console.log('result : ', result);
    });
   
  }
  toggleLoadingAnimation() {
    this.loading = true;
    // this.dialog.closeAll();
    // const dialogRef = this.dialog.open(RegistreComponent, {
    //   width: '1000px',
    //   height: '600px',
    //   data: {
    //     title: 'Attention !',
    //   }
    // });
    setTimeout(() => this.loading = false, 3000);
  }
}
