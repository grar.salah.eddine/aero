import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
// import "@angular/compiler"
import { NgxAuthRoutingModule } from './auth-routing.module';
import { NbAuthJWTToken, NbAuthModule, NbPasswordAuthStrategy } from '@nebular/auth';
import { 
  NbAlertModule,
  NbButtonModule,
  NbCheckboxModule,
  NbInputModule,
  NbSpinnerModule
} from '@nebular/theme';
import { NgxLoginComponent } from './login/login.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { CredentialsInterceptor } from '../services/Credentiel.interceptor';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    NbAlertModule,
    NbInputModule,
    NbButtonModule,
    NbCheckboxModule,
    NgxAuthRoutingModule,
    NbAuthModule.forRoot({
      strategies: [
        NbPasswordAuthStrategy.setup({
          name: 'email',
          baseEndpoint: 'http://localhost:1337',
          token: {
            class: NbAuthJWTToken,
            key: 'jwt',
          },
          login: {
            endpoint: '/auth/local',
            method: 'POST',
            defaultErrors: ['Login/Email combination is not correct, please try again.'],
            defaultMessages: ['You have been successfully logged in.'],
          }
        }),
        
      ],
      forms: {
      }
    }), 
    ReactiveFormsModule,
    NbSpinnerModule,
    
  ],
  declarations: [
    NgxLoginComponent
    // ... here goes our new components
  ],
  providers: [
    // {provide : HTTP_INTERCEPTORS ,
    // useClass : CredentialsInterceptor,
    // multi : true
    // }
  ]
})
export class NgxAuthModule {
}