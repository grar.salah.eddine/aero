import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbThemeModule, NbLayoutModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http'
import { NbAuthJWTToken, NbAuthService, NbAuthSimpleToken, NbAuthStrategyOptions ,} from '@nebular/auth';
import { ExtraOptions } from '@angular/router';
import { NbPasswordAuthStrategy, NbAuthModule } from '@nebular/auth';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CredentialsInterceptor } from './services/Credentiel.interceptor';
const config: ExtraOptions = {
  useHash: false,
};

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NbThemeModule.forRoot({ name: 'default' }),
    NbLayoutModule,
    NbEvaIconsModule,
    HttpClientModule,
    NbAuthModule.forRoot({
      strategies: [
        NbPasswordAuthStrategy.setup({
          name: 'email',
          token: {
            class: NbAuthJWTToken,
            key: 'jwt',
          },
          baseEndpoint: 'http://localhost:1337',
          login: {
            endpoint: '/auth/local',
          },
        }),
      ],
      forms: {
        
      },
    }), 
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [ {provide : HTTP_INTERCEPTORS ,
    useClass : CredentialsInterceptor,
    multi : true
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
