import { Component, OnInit } from '@angular/core';
import { NbAuthJWTToken, NbAuthService, NbTokenService } from '@nebular/auth';
import { ArticlesService } from './services/articles.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'aerodrone';

   userActive : any = [];
  constructor(private userService: ArticlesService, private NbTokenService :NbTokenService , private authService:NbAuthService){

  }
  async ngOnInit(){
    this.userActive = await this.NbTokenService.get().toPromise();
    this.authService.onTokenChange()
    .subscribe((token: NbAuthJWTToken) => {
      console.log('token :',token);
      if (token.isValid()) {
        this.userActive = token.getPayload(); // here we receive a payload from the token and assigns it to our `user` variable 
      }
      console.log('user :',this.userActive);
    });

  }

}
