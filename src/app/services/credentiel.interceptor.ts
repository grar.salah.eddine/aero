import { Observable } from 'rxjs';

import { HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
// import { request } from 'http';


@Injectable()
export class CredentialsInterceptor implements HttpInterceptor {

  constructor() { }

  intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    if (req.method.toLowerCase() === 'post' && req.url ==="http://localhost:1337/auth/local") {
        req =  req.clone({
          body: { identifier: req.body['email'], password: req.body['password'] }
        })
    }

    return next.handle(req);
  }
  
//   handleBodyIn(req:HttpRequest<any>, identifier) {
//       console.log('kdaaaaaaaaaaaaaaaaaaaaaa:',req);
//     if (req.method.toLowerCase() === 'post' && req.url ==="http://localhost:1337/auth/local") {
//         req =  req.clone({
//           body: req.body.append(identifier,  req.body.email)
//         })
   
//     return req;    
//   }
}

